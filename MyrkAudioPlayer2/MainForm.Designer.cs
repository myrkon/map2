﻿/*
 * Myrk created this class in 02.01.2019
 */
namespace MyrkAudioPlayer2
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button playBtn;
		private System.Windows.Forms.Button pauseBtn;
		private System.Windows.Forms.Button stopBtn;
		private System.Windows.Forms.Button nextSongBtn;
		private System.Windows.Forms.Button previousSongBtn;
		private System.Windows.Forms.TrackBar volumeBar;
		private System.Windows.Forms.TrackBar trackTimeBar;
		private System.Windows.Forms.Label currentPosition;
		private System.Windows.Forms.Label endPosition;
		private System.Windows.Forms.ListBox playl;
		private System.Windows.Forms.Button addTrackBtn;
		private System.Windows.Forms.Button removeTrackBtn;
		private System.Windows.Forms.Button loadBtn;
		private System.Windows.Forms.Button saveBtn;
		private System.Windows.Forms.OpenFileDialog ofd;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.OpenFileDialog ofd1;
		private System.Windows.Forms.SaveFileDialog sfd;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.playBtn = new System.Windows.Forms.Button();
			this.pauseBtn = new System.Windows.Forms.Button();
			this.stopBtn = new System.Windows.Forms.Button();
			this.nextSongBtn = new System.Windows.Forms.Button();
			this.previousSongBtn = new System.Windows.Forms.Button();
			this.volumeBar = new System.Windows.Forms.TrackBar();
			this.trackTimeBar = new System.Windows.Forms.TrackBar();
			this.currentPosition = new System.Windows.Forms.Label();
			this.endPosition = new System.Windows.Forms.Label();
			this.playl = new System.Windows.Forms.ListBox();
			this.addTrackBtn = new System.Windows.Forms.Button();
			this.removeTrackBtn = new System.Windows.Forms.Button();
			this.loadBtn = new System.Windows.Forms.Button();
			this.saveBtn = new System.Windows.Forms.Button();
			this.ofd = new System.Windows.Forms.OpenFileDialog();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.ofd1 = new System.Windows.Forms.OpenFileDialog();
			this.sfd = new System.Windows.Forms.SaveFileDialog();
			((System.ComponentModel.ISupportInitialize)(this.volumeBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackTimeBar)).BeginInit();
			this.SuspendLayout();
			// 
			// playBtn
			// 
			this.playBtn.Location = new System.Drawing.Point(12, 12);
			this.playBtn.Name = "playBtn";
			this.playBtn.Size = new System.Drawing.Size(24, 23);
			this.playBtn.TabIndex = 0;
			this.playBtn.Text = "|>";
			this.playBtn.UseVisualStyleBackColor = true;
			this.playBtn.Click += new System.EventHandler(this.PlayBtnClick);
			// 
			// pauseBtn
			// 
			this.pauseBtn.Location = new System.Drawing.Point(42, 12);
			this.pauseBtn.Name = "pauseBtn";
			this.pauseBtn.Size = new System.Drawing.Size(24, 23);
			this.pauseBtn.TabIndex = 1;
			this.pauseBtn.Text = "| |";
			this.pauseBtn.UseVisualStyleBackColor = true;
			this.pauseBtn.Click += new System.EventHandler(this.PauseBtnClick);
			// 
			// stopBtn
			// 
			this.stopBtn.Location = new System.Drawing.Point(72, 12);
			this.stopBtn.Name = "stopBtn";
			this.stopBtn.Size = new System.Drawing.Size(24, 23);
			this.stopBtn.TabIndex = 2;
			this.stopBtn.Text = "[ ]";
			this.stopBtn.UseVisualStyleBackColor = true;
			this.stopBtn.Click += new System.EventHandler(this.StopBtnClick);
			// 
			// nextSongBtn
			// 
			this.nextSongBtn.Location = new System.Drawing.Point(136, 12);
			this.nextSongBtn.Name = "nextSongBtn";
			this.nextSongBtn.Size = new System.Drawing.Size(28, 23);
			this.nextSongBtn.TabIndex = 3;
			this.nextSongBtn.Text = ">>";
			this.nextSongBtn.UseVisualStyleBackColor = true;
			this.nextSongBtn.Click += new System.EventHandler(this.NextSongBtnClick);
			// 
			// previousSongBtn
			// 
			this.previousSongBtn.Location = new System.Drawing.Point(102, 12);
			this.previousSongBtn.Name = "previousSongBtn";
			this.previousSongBtn.Size = new System.Drawing.Size(28, 23);
			this.previousSongBtn.TabIndex = 4;
			this.previousSongBtn.Text = "<<";
			this.previousSongBtn.UseVisualStyleBackColor = true;
			this.previousSongBtn.Click += new System.EventHandler(this.PreviousSongBtnClick);
			// 
			// volumeBar
			// 
			this.volumeBar.Location = new System.Drawing.Point(170, 12);
			this.volumeBar.Maximum = 100;
			this.volumeBar.Name = "volumeBar";
			this.volumeBar.Size = new System.Drawing.Size(104, 45);
			this.volumeBar.TabIndex = 5;
			this.volumeBar.TickStyle = System.Windows.Forms.TickStyle.None;
			this.volumeBar.Value = 100;
			this.volumeBar.Scroll += new System.EventHandler(this.VolumeBarScroll);
			// 
			// trackTimeBar
			// 
			this.trackTimeBar.Location = new System.Drawing.Point(12, 65);
			this.trackTimeBar.Name = "trackTimeBar";
			this.trackTimeBar.Size = new System.Drawing.Size(262, 45);
			this.trackTimeBar.TabIndex = 6;
			this.trackTimeBar.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackTimeBar.Scroll += new System.EventHandler(this.TrackTimeBarScroll);
			// 
			// currentPosition
			// 
			this.currentPosition.Location = new System.Drawing.Point(12, 48);
			this.currentPosition.Name = "currentPosition";
			this.currentPosition.Size = new System.Drawing.Size(36, 14);
			this.currentPosition.TabIndex = 7;
			this.currentPosition.Text = "00:00";
			// 
			// endPosition
			// 
			this.endPosition.Location = new System.Drawing.Point(236, 48);
			this.endPosition.Name = "endPosition";
			this.endPosition.Size = new System.Drawing.Size(36, 14);
			this.endPosition.TabIndex = 8;
			this.endPosition.Text = "00:00";
			// 
			// playl
			// 
			this.playl.FormattingEnabled = true;
			this.playl.Items.AddRange(new object[] {
			""});
			this.playl.Location = new System.Drawing.Point(12, 130);
			this.playl.Name = "playl";
			this.playl.Size = new System.Drawing.Size(260, 121);
			this.playl.TabIndex = 9;
			this.playl.DoubleClick += new System.EventHandler(this.PlaylDoubleClick);
			// 
			// addTrackBtn
			// 
			this.addTrackBtn.Location = new System.Drawing.Point(12, 101);
			this.addTrackBtn.Name = "addTrackBtn";
			this.addTrackBtn.Size = new System.Drawing.Size(24, 23);
			this.addTrackBtn.TabIndex = 10;
			this.addTrackBtn.Text = "+";
			this.addTrackBtn.UseVisualStyleBackColor = true;
			this.addTrackBtn.Click += new System.EventHandler(this.AddTrackBtnClick);
			// 
			// removeTrackBtn
			// 
			this.removeTrackBtn.Location = new System.Drawing.Point(42, 101);
			this.removeTrackBtn.Name = "removeTrackBtn";
			this.removeTrackBtn.Size = new System.Drawing.Size(24, 23);
			this.removeTrackBtn.TabIndex = 11;
			this.removeTrackBtn.Text = "-";
			this.removeTrackBtn.UseVisualStyleBackColor = true;
			this.removeTrackBtn.Click += new System.EventHandler(this.RemoveTrackBtnClick);
			// 
			// loadBtn
			// 
			this.loadBtn.Location = new System.Drawing.Point(89, 101);
			this.loadBtn.Name = "loadBtn";
			this.loadBtn.Size = new System.Drawing.Size(41, 23);
			this.loadBtn.TabIndex = 12;
			this.loadBtn.Text = "load";
			this.loadBtn.UseVisualStyleBackColor = true;
			this.loadBtn.Click += new System.EventHandler(this.LoadBtnClick);
			// 
			// saveBtn
			// 
			this.saveBtn.Location = new System.Drawing.Point(136, 101);
			this.saveBtn.Name = "saveBtn";
			this.saveBtn.Size = new System.Drawing.Size(41, 23);
			this.saveBtn.TabIndex = 13;
			this.saveBtn.Text = "save";
			this.saveBtn.UseVisualStyleBackColor = true;
			this.saveBtn.Click += new System.EventHandler(this.SaveBtnClick);
			// 
			// ofd
			// 
			this.ofd.Filter = "MP3|*.mp3";
			this.ofd.Multiselect = true;
			// 
			// timer1
			// 
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// ofd1
			// 
			this.ofd1.Filter = "Playlists (*.json)|*.json";
			this.ofd1.Multiselect = true;
			// 
			// sfd
			// 
			this.sfd.Filter = "Playlist(*.json)|*.json";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.saveBtn);
			this.Controls.Add(this.loadBtn);
			this.Controls.Add(this.removeTrackBtn);
			this.Controls.Add(this.addTrackBtn);
			this.Controls.Add(this.playl);
			this.Controls.Add(this.endPosition);
			this.Controls.Add(this.currentPosition);
			this.Controls.Add(this.trackTimeBar);
			this.Controls.Add(this.volumeBar);
			this.Controls.Add(this.previousSongBtn);
			this.Controls.Add(this.nextSongBtn);
			this.Controls.Add(this.stopBtn);
			this.Controls.Add(this.pauseBtn);
			this.Controls.Add(this.playBtn);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximumSize = new System.Drawing.Size(300, 300);
			this.MinimumSize = new System.Drawing.Size(300, 300);
			this.Name = "MainForm";
			this.Text = "MyrkAudioPlayer2";
			((System.ComponentModel.ISupportInitialize)(this.volumeBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackTimeBar)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
