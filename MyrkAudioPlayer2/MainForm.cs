﻿/*
 * Myrk created this class in 02.01.2019
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using MyrkAudioLibrary;
using System.IO;
using System.Text.RegularExpressions;
namespace MyrkAudioPlayer2
{
	public partial class MainForm : Form{
		public MainForm(){
			InitializeComponent();
			MyrkAudioLibrary.AudioLibrary.Init();
			
			playl.Items.Clear();
			string[] args = Environment.GetCommandLineArgs();
			
			if(args.Length > 1){
				for (int i = 1; i < args.Length; i++) {
					playl.Items.Add(v.GetFileName(args[i]));
					v.files.Add(args[i]);
				}
				
				playl.SelectedIndex = 0;
				PlaySong();
			}
			
			if(File.Exists(v.pathToSettings) ){	
				string readedSettings = "";
				try{
					var sr = new StreamReader(v.pathToSettings);
					readedSettings = sr.ReadToEnd();
					sr.Close();
				}catch(IOException ex){;}
				var set	= JsonConvert.DeserializeObject<Settings>(readedSettings);
				//setup the application
				volumeBar.Value = set.defaultVolume;
				if(set.autoloadPlaylistEnable && args.Length<2){
					if(set.autoloadPlaylistPath.Contains("\\")) set.autoloadPlaylistPath = Regex.Replace(set.autoloadPlaylistPath, @"\\", @"\\");
					LoadPlaylistFromSettings(set.autoloadPlaylistPath);
				}
				if(set.autoplayEnable && set.autoloadPlaylistEnable && args.Length<2){
					playl.SelectedIndex = set.autoplayDefaultIndex;
					timer1.Enabled = true;
					PlaySong();
				}
				
			}else{
				string serializableSettings = "";
				var set = new Settings();
				try{
					var sw = new StreamWriter(v.pathToSettings);
					serializableSettings = JsonConvert.SerializeObject(set, Formatting.Indented);
					sw.Write(serializableSettings);
					sw.Close();
				}catch(IOException ex){;}
			}
			
//			timer1.Enabled = true;
			
		}
		void AddTrackBtnClick(object sender, EventArgs e){
			try{
				ofd.ShowDialog();
			}catch(Exception ex){;}
			string[] vagina = ofd.FileNames;
			
			for (int i = 0; i < vagina.Length; i++) {
				playl.Items.Add(v.GetFileName(vagina[i]));
				v.files.Add(vagina[i]);
			}
			
			playl.SelectedIndex = 0;
		}
		
		void PlaySong(){
			timer1.Enabled = true;
			if((playl.SelectedIndex != -1)&& (playl.Items.Count != 0)){
				try{
					MyrkAudioLibrary.AudioLibrary.Play(v.files[playl.SelectedIndex], volumeBar.Value);
//					MyrkAudioLibrary.AudioLibrary.Play_ver2(v.files[pl.SelectedIndex], volumeBar.Value);
					trackTimeBar.Maximum = MyrkAudioLibrary.AudioLibrary.GetTime();
					trackTimeBar.Value = MyrkAudioLibrary.AudioLibrary.GetCurrPos();
					currentPosition.Text = TimeSpan.FromMinutes(MyrkAudioLibrary.AudioLibrary.GetCurrPos()).ToString();
					endPosition.Text = TimeSpan.FromMinutes(MyrkAudioLibrary.AudioLibrary.GetTime()).ToString();
					
				}catch(Exception ex){}
			}
		}
		
		
		
		void PlayBtnClick(object sender, EventArgs e){
			PlaySong();
//			PlaySong_ver2();
			
				
		}
		void RemoveTrackBtnClick(object sender, EventArgs e){
			int previ = 0;
            if(playl.SelectedIndex != -1 && playl.Items.Count > 1)
            {
                try{
                    previ = playl.SelectedIndex;
                    v.files.RemoveAt(playl.SelectedIndex);
                    playl.Items.RemoveAt(playl.SelectedIndex);
                    playl.SelectedIndex = previ;
                }catch (System.IndexOutOfRangeException ex)
                {MessageBox.Show("Ошибка в программе. выход из границ массива \n\n" + ex.Message);}

            }
            PlaySong();
		}
		void StopBtnClick(object sender, EventArgs e){
			MyrkAudioLibrary.AudioLibrary.Stop();
			timer1.Enabled = false;
		}
		void NextSongBtnClick(object sender, EventArgs e){
			playl.SelectedIndex++;
			PlaySong();
		}
		void PreviousSongBtnClick(object sender, EventArgs e){
			playl.SelectedIndex--;
			PlaySong();
		}
		void Timer1Tick(object sender, EventArgs e)
		{
			trackTimeBar.Maximum = MyrkAudioLibrary.AudioLibrary.GetTime();
			trackTimeBar.Value = MyrkAudioLibrary.AudioLibrary.GetCurrPos();
			currentPosition.Text = TimeSpan.FromMinutes(MyrkAudioLibrary.AudioLibrary.GetCurrPos()).ToString();
			if(currentPosition.Text == endPosition.Text){
				try{
					playl.SelectedIndex++;
					PlaySong();
				}catch(Exception ex){}
			}
		}
		void TrackTimeBarScroll(object sender, EventArgs e)
		{
			MyrkAudioLibrary.AudioLibrary.SetPosition(trackTimeBar.Value);
		}
		void PauseBtnClick(object sender, EventArgs e)
		{
			timer1.Enabled = false;
			MyrkAudioLibrary.AudioLibrary.Pause();
		}
		
		void LoadPlaylistFromSettings(string pathToPlaylist){
			string readedFile = "";
			try{
				var sr = new StreamReader(pathToPlaylist);
				readedFile = sr.ReadToEnd();
				sr.Close();
			}catch(IOException ex){MessageBox.Show(ex.Message);}
			
//			if(readedFile.Contains("\\")){
				readedFile = Regex.Replace(readedFile, @"\\", @"\\");
//			}
			
			PlayList pl = JsonConvert.DeserializeObject<PlayList>(readedFile);
			playl.Items.Clear();
			v.files.Clear();
			for (int i = 0; i < pl.pl.Count; i++) {
				if(File.Exists(pl.pl[i])){
					playl.Items.Add(Path.GetFileNameWithoutExtension(pl.pl[i]));
					v.files.Add(pl.pl[i]);
				}else{
					MessageBox.Show(pl.pl[i]+" doesn't exists");
				}
			}
			
		}
		
		void LoadBtnClick(object sender, EventArgs e)
		{
			try{
				ofd1.ShowDialog();
			}catch(Exception ex){;}
			string readedFile = "";
			try{
				var sr = new StreamReader(ofd1.FileName);
				readedFile = sr.ReadToEnd();
				sr.Close();
			}catch(IOException ex){MessageBox.Show(ex.Message);}
			
//			if(readedFile.Contains("\\")){
				readedFile = Regex.Replace(readedFile, @"\\", @"\\");
//			}
			
			PlayList pl = JsonConvert.DeserializeObject<PlayList>(readedFile);
			playl.Items.Clear();
			v.files.Clear();
			for (int i = 0; i < pl.pl.Count; i++) {
				if(File.Exists(pl.pl[i])){
					playl.Items.Add(Path.GetFileNameWithoutExtension(pl.pl[i]));
					v.files.Add(pl.pl[i]);
				}else{
					MessageBox.Show(pl.pl[i]+" doesn't exists");
				}
			}
			
			playl.SelectedIndex = 0;
			PlaySong();
		}
		void SaveBtnClick(object sender, EventArgs e)
		{
			try{
				sfd.ShowDialog();
			}catch(Exception ex){;}
			var pl = new PlayList();
			pl.pl = v.files;
			string serializedPlaylist = JsonConvert.SerializeObject(pl, Formatting.Indented);
			
			try{
				var sw = new StreamWriter(sfd.FileName);
				sw.Write(serializedPlaylist);
				sw.Close();
			}catch(IOException ex){MessageBox.Show(ex.Message);}
		}
		void VolumeBarScroll(object sender, EventArgs e)
		{
			MyrkAudioLibrary.AudioLibrary.SetVolume(volumeBar.Value);
		}
		void PlaylDoubleClick(object sender, EventArgs e)
		{
			PlaySong();
		}
	}
	
	class PlayList{
		public List<string> pl = new List<string>();
	}
	
	class Settings{
		public string autoloadPlaylistPath = "";
		public bool autoloadPlaylistEnable = false;
		public bool autoplayEnable=false;
		public int autoplayDefaultIndex = 0;
		public int defaultVolume = 100;
	}
}
